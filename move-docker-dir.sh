#!/bin/bash

file=/etc/docker/daemon.json

if [ ! -f "$file" ]; then
    sudo touch $file
fi

if [[ -z $(grep '[^[:space:]]' < "$file") ]]; then
    echo "{}" | sudo tee "$file" > /dev/null
fi

destdir="$HOME/docker"
mkdir -p $destdir

sudo service docker stop

cat $file | jq --arg dir "$destdir" '. += {"data-root": $dir}' | sudo tee "$file" > /dev/null
if [ -d /var/lib/docker ]; then
    sudo rsync -aP /var/lib/docker/ "$destdir"
    sudo mv /var/lib/docker /var/lib/docker.old
fi

sudo service docker start

# post docker installation
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
